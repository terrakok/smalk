package io.terrakok.smalk.service

import kotlin.js.Date

@JsModule("dateformat")
@JsNonModule
private external object dateFormat {
    fun default(
        date: dynamic,
        mask: String,
        utc: Boolean = definedExternally,
        gmt: Boolean = definedExternally
    ): String
}

fun jsFormatDate(date: Date, pattern: String): String = dateFormat.default(date, pattern)