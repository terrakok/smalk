package io.terrakok.smalk.ui.home

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.*
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import io.terrakok.smalk.LocalAppScope
import io.terrakok.smalk.service.SessionManager
import io.terrakok.smalk.theme.LocalThemeIsDark
import io.terrakok.smalk.ui.Avatar
import io.terrakok.smalk.ui.auth.LoginScreen
import io.terrakok.smalk.ui.verification.SelfVerificationDialog
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.verification
import net.folivo.trixnity.client.verification.VerificationService
import net.folivo.trixnity.clientserverapi.client.SyncState

@Composable
fun HomeTopBar(sessionManager: SessionManager, syncState: SyncState) {
    val client = sessionManager.getClient()
    val navigator = LocalNavigator.currentOrThrow
    val appScope = LocalAppScope.current
    CenterAlignedTopAppBar(
        title = {
            val stateName = when (syncState) {
                SyncState.INITIAL_SYNC -> "Connecting..."
                SyncState.STARTED, SyncState.RUNNING, SyncState.TIMEOUT -> "Smalk"
                SyncState.ERROR -> "No connection..."
                SyncState.STOPPING -> "Stopping"
                SyncState.STOPPED -> "Stopped"
            }
            Text(
                stateName,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
        navigationIcon = {
            var showLogoutDialog by remember { mutableStateOf(false) }
            if (showLogoutDialog) {
                AlertDialog(
                    onDismissRequest = { showLogoutDialog = false },
                    text = { Text("Do you want to logout?") },
                    confirmButton = {
                        Button(onClick = {
                            showLogoutDialog = false
                            appScope.launch {
                                delay(50) //TODO fix racing on state text
                                sessionManager.logout()
                            }
                            navigator.replaceAll(LoginScreen())
                        }) {
                            Text("Logout")
                        }
                    },
                    dismissButton = {
                        Button(onClick = {
                            showLogoutDialog = false
                        }) {
                            Text("Cancel")
                        }
                    },
                )
            }
            IconButton(onClick = { showLogoutDialog = true }) {
                ProfileAvatar(client = client)
            }
        },
        actions = {
            val verificationState by client.verification
                .getSelfVerificationMethods()
                .filter { it !is VerificationService.SelfVerificationMethods.PreconditionsNotMet }
                .collectAsState(null)
            val verification = verificationState
            if (verification is VerificationService.SelfVerificationMethods.CrossSigningEnabled) {
                var showVerificationDialog by remember { mutableStateOf(false) }
                IconButton(
                    onClick = { showVerificationDialog = true }
                ) {
                    Icon(
                        modifier = Modifier.padding(8.dp),
                        imageVector = Icons.Outlined.Key,
                        tint = MaterialTheme.colorScheme.error,
                        contentDescription = null
                    )
                }
                if (showVerificationDialog) {
                    SelfVerificationDialog(
                        verification.methods,
                        onDismissRequest = { showVerificationDialog = false }
                    )
                }
            }

            var isDark by LocalThemeIsDark.current
            IconButton(onClick = { isDark = !isDark }) {
                Icon(
                    modifier = Modifier.padding(8.dp),
                    imageVector = if (isDark) Icons.Outlined.LightMode else Icons.Outlined.DarkMode,
                    contentDescription = null
                )
            }
        }
    )
}

@Composable
private fun ProfileAvatar(
    modifier: Modifier = Modifier,
    client: MatrixClient,
    textSize: TextUnit = 18.sp
) {
    val urlAndName by combine(client.avatarUrl, client.displayName) { u, n -> u to n }
        .collectAsState(null to null)
    val (url, name) = urlAndName
    Avatar(
        modifier = modifier,
        client = client,
        id = client.userId.full,
        url = url,
        name = name.orEmpty(),
        textSize = textSize
    )
}