package io.terrakok.smalk.ui.auth

import Smalk.composeApp.BuildConfig
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Visibility
import androidx.compose.material.icons.outlined.VisibilityOff
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import io.terrakok.smalk.service.SessionManager
import io.terrakok.smalk.ui.home.HomeScreen
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class LoginScreen: Screen, KoinComponent {
    override val key = uniqueScreenKey

    @Composable
    override fun Content() {
        val sessionManager by inject<SessionManager>()
        val scope = rememberCoroutineScope()
        val navigator = LocalNavigator.currentOrThrow
        var server by remember { mutableStateOf("matrix.org") }
        var username by remember { mutableStateOf(BuildConfig.DEFAULT_USER) }
        var password by remember { mutableStateOf(BuildConfig.DEFAULT_PWD) }

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            OutlinedCard(
                modifier = Modifier.width(400.dp).padding(24.dp)
            ) {
                Column(
                    modifier = Modifier.padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        value = server,
                        onValueChange = { server = it },
                        label = { Text("Homeserver") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    OutlinedTextField(
                        value = username,
                        onValueChange = { username = it },
                        label = { Text("Username") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    PasswordTextField(
                        password = password,
                        onPasswordChange = { password = it },
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.size(16.dp))

                    var isProgress by remember { mutableStateOf(false) }
                    var showMessageDialog by remember { mutableStateOf(false) }
                    var message by remember { mutableStateOf("") }

                    if (showMessageDialog) {
                        AlertDialog(
                            onDismissRequest = { showMessageDialog = false },
                            text = { Text(message) },
                            confirmButton = {
                                TextButton(
                                    onClick = { showMessageDialog = false }
                                ) {
                                    Text("OK")
                                }
                            }
                        )
                    }

                    ProgressButton(
                        modifier = Modifier.align(Alignment.End),
                        enabled = server.isNotBlank() && username.isNotBlank() && password.isNotBlank(),
                        progress = isProgress,
                        text = "Login",
                        onClick = {
                            scope.launch {
                                isProgress = true
                                try {
                                    sessionManager.login(server, username, password)
                                    navigator.replace(HomeScreen())
                                } catch (e: Exception) {
                                    message = e.message.orEmpty()
                                    showMessageDialog = true
                                }
                                isProgress = false
                            }
                        }
                    )
                }
            }
        }
    }

    @Composable
    private fun PasswordTextField(
        password: String,
        onPasswordChange: (String) -> Unit,
        modifier: Modifier = Modifier,
        label: String = "Password"
    ) {
        var passwordVisibility by remember { mutableStateOf(false) }

        OutlinedTextField(
            value = password,
            onValueChange = onPasswordChange,
            modifier = modifier,
            maxLines = 1,
            singleLine = true,
            label = { Text(label) },
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password
            ),
            trailingIcon = {
                IconButton(
                    modifier = Modifier.size(24.dp),
                    onClick = { passwordVisibility = !passwordVisibility }
                ) {
                    val imageVector =
                        if (passwordVisibility) Icons.Outlined.Visibility else Icons.Outlined.VisibilityOff
                    Icon(imageVector, contentDescription = if (passwordVisibility) "Hide password" else "Show password")
                }
            }
        )
    }

    @Composable
    private fun ProgressButton(
        modifier: Modifier,
        text: String,
        progress: Boolean,
        enabled: Boolean,
        onClick: () -> Unit
    ) {
        Button(
            modifier = modifier,
            onClick = onClick,
            enabled = enabled && !progress
        ) {
            if (progress) {
                CircularProgressIndicator(
                    modifier = Modifier.size(16.dp),
                    strokeWidth = 1.dp
                )
                Spacer(Modifier.size(ButtonDefaults.IconSpacing))
            }
            Text(text)
        }
    }
}