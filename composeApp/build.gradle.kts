import java.util.*

plugins {
    alias(libs.plugins.multiplatform)
    alias(libs.plugins.compose)
    alias(libs.plugins.android.application)
    alias(libs.plugins.buildConfig)
    alias(libs.plugins.dependencyUpdates)
}

val localProps = Properties()
if (rootProject.file("local.properties").exists()) {
    localProps.load(rootProject.file("local.properties").inputStream())
}
val isDebug = localProps.getOrDefault("debug", "false") == "true"
val defUser = localProps.getOrDefault("user", "").toString()
val defPassword = localProps.getOrDefault("password", "").toString()

kotlin {
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = "11"
            }
        }
    }

    jvm()

    js {
        browser {
            commonWebpackConfig {
                configDirectory = rootDir.resolve("webpack.config.d")
            }
        }
        binaries.executable()
    }

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach { iosTarget ->
        iosTarget.binaries.framework {
            baseName = "ComposeApp"
            isStatic = true
        }
    }

    sourceSets {
        all {
            languageSettings {
                optIn("kotlinx.coroutines.ExperimentalCoroutinesApi")
                optIn("androidx.compose.material3.ExperimentalMaterial3Api")
            }
        }
        commonMain {
            dependencies {
                implementation(compose.runtime)
                implementation(compose.material3)
                implementation(compose.materialIconsExtended)
                implementation(libs.voyager.navigator)
                implementation(libs.napier)
                implementation(libs.kotlinx.coroutines.core)
                implementation(libs.kotlinx.datetime)
                implementation(libs.koin.core)
                implementation(libs.trixnity.client)
                implementation(libs.ktor.client.core)
                implementation(libs.ktor.logging)
                implementation(libs.multiplatform.settings)
            }
        }

        commonTest {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        androidMain {
            dependencies {
                implementation(libs.androidx.appcompat)
                implementation(libs.androidx.activityCompose)
                implementation(libs.kotlinx.coroutines.android)
                implementation(libs.ktor.client.okhttp)
                implementation(libs.trixnity.client.repository.realm)
                implementation(libs.trixnity.client.media.okio)
            }
        }

        jvmMain {
            dependencies {
                implementation(compose.desktop.common)
                implementation(compose.desktop.currentOs)
                implementation(libs.ktor.client.okhttp)
                implementation(libs.kotlinx.coroutines.swing)
                implementation(libs.trixnity.client.repository.realm)
                implementation(libs.trixnity.client.media.okio)
                if (isDebug) implementation(libs.logback.classic)
            }
        }

        iosMain {
            dependencies {
                implementation(libs.ktor.client.darwin)
                implementation(libs.trixnity.client.repository.realm)
                implementation(libs.trixnity.client.media.okio)
            }
        }

        jsMain {
            dependencies {
                implementation(libs.ktor.client.js)
                implementation(libs.trixnity.client.repository.indexeddb)
                implementation(libs.trixnity.client.media.indexeddb)
                implementation(npm("copy-webpack-plugin", libs.versions.copyWebpackPlugin.get()))
                implementation(npm("dateformat", "5.0.3"))
            }
        }
    }
}

android {
    namespace = "io.terrakok.smalk"
    compileSdk = 34

    defaultConfig {
        minSdk = 24
        targetSdk = 34

        applicationId = "io.terrakok.smalk.androidApp"
        versionCode = 1
        versionName = "1.0.0"
    }
    sourceSets["main"].apply {
        manifest.srcFile("src/androidMain/AndroidManifest.xml")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    packaging {
        resources {
            excludes += "META-INF/**"
        }
    }
    buildTypes {
        getByName("release") {
            signingConfig = signingConfigs.getByName("debug")
        }
    }
}

compose {
    desktop {
        application {
            mainClass = "MainKt"

            nativeDistributions {
                targetFormats(
                    org.jetbrains.compose.desktop.application.dsl.TargetFormat.Dmg,
                    org.jetbrains.compose.desktop.application.dsl.TargetFormat.Deb
                )
                packageName = "Smalk"
                packageVersion = "1.0.0"
                val iconsRoot = project.file("src/desktopMain/resources/icons/")

                macOS {
                    iconFile.set(iconsRoot.resolve("AppIcon.icns"))
                    packageName = "Smalk"
                    bundleID = "io.terrakok.smalk"
                }
                linux {
                    iconFile.set(iconsRoot.resolve("AppIcon.png"))
                }
            }
        }
    }
    experimental {
        web.application {}
    }
}

buildConfig {
    // BuildConfig configuration here.
    // https://github.com/gmazzo/gradle-buildconfig-plugin#usage-in-kts
    useKotlinOutput { internalVisibility = true }
    buildConfigField("boolean", "DEBUG", "$isDebug")
    buildConfigField("String", "DEFAULT_USER", "\"${defUser}\"")
    buildConfigField("String", "DEFAULT_PWD", "\"${defPassword}\"")
}
